import csv
import curses
import logging
import os
import random
import threading
import time
from decimal import *

from Queue import LifoQueue

from console import Console


class App(object):

    def __init__(self, stdscr):
        self.csv_path = ''
        self.console = ''
        self.data = {}
        self.time_interval = 1
        self.stdscr = stdscr
        self.currency_pair_queue = LifoQueue()
        self.logger = logging.getLogger(__name__)
        self.price_running = False
        self.operations = [' BUYS ', ' SELLS ']
        self.view_mode = 0
        self.logger = logging.getLogger(__name__)

    def enum(**enums):
        return type('Enum', (), enums)

    def read_data(self):
        try:
            f = open(self.csv_path)
            mycsv = csv.reader(f, delimiter=';')
            self.data = {line[0]: (Decimal(line[1]), Decimal(line[2])) for line in mycsv}
            f.close()
            self.data['USD'] = (1, 0)  # pivot currency in system
            self.console.add_line('File was read successfully!', 2)
            self.logger.info('File "%s" read successfully: ', self.csv_path)
            return 1
        except IOError:
            self.console.add_line('Wrong file! Input another one, please.', 2)
            self.logger.error('File "%s" reading failed: ', self.csv_path)
            return 0

    def price_update(self):

        while True:
            if self.price_running:
                currency_pair = self.currency_pair_queue.get()
                # self.currency_pair_queue.put(currency_pair)
                if currency_pair != '':
                    pair_name = currency_pair[0][0] + currency_pair[1][0]
                    random.seed()
                    deviation1 = currency_pair[0][2]
                    deviation2 = currency_pair[1][2]
                    price1 = currency_pair[0][1]
                    price2 = currency_pair[1][1]
                    new_price1 = Decimal(random.uniform(float(price1 - deviation1), float(price1 + deviation1)))
                    new_price2 = Decimal(random.uniform(float(price2 - deviation2), float(price2 + deviation2)))
                    currency_pair[0][1] = new_price1
                    currency_pair[1][1] = new_price2
                    if (new_price1 < Decimal('0')) or (new_price2 < Decimal('0')):
                        self.console.add_line('Fatal error. Negative currency rate. Terminating...', 1)
                        # TODO: log last Queue elements with trace
                        self.logger.critical('Negative currency rate')
                        time.sleep(5)
                        sys.exit(1)
                    else:
                        price_per_pair = Decimal(new_price2) / Decimal(new_price1)
                        self.currency_pair_queue.put(currency_pair)
                        #TODO: log cost of 1st and 2nd currency for testing the moment of "BUY" and "SELL"
                        self.logger.info('New currecy pair %s was generated: ' + str(new_price1) + ' ' + str(new_price2), currency_pair  )
                        # self.console.add_line(str(new_price1) + ' ' + str(deviation1), 1)
                        # self.console.add_line(str(new_price2) + ' ' + str(deviation2), 1)
                        self.console.add_line(str(price_per_pair) + ' per pair ' + pair_name, 0)
                    time.sleep(self.time_interval)
                # uncomment, if want to save price history
                # self.currency_pair_queue.put(currency_pair)

    def cmd_list(self):
        self.read_data()  # optional, can be deleted, if file never changes
        for key in self.data:
            for key2 in self.data:
                if key != key2:
                    self.console.add_line(key + key2, 1)

    def cmd_price(self, currency_pair_name):
        if not self.price_running:
            curr1 = currency_pair_name[0:3]
            curr2 = currency_pair_name[3:6]
            if self.data.has_key(curr1) & self.data.has_key(curr2) & (curr1 != curr2):
                currency_pair = [[curr1, self.data[curr1][0], self.data[curr1][1]],
                                 [curr2, self.data[curr2][0], self.data[curr2][1]]]
                self.currency_pair_queue.put(currency_pair)
                self.price_running = True
            else:
                self.logger.warning('Error. This currency pair is absent')
                self.console.add_line('Error. This currency pair is absent!', 2)
        else:
            self.logger.warning('Error. STOP actual current pair first!')
            self.console.add_line('Error. STOP actual current pair first!', 2)

    def cmd_stop(self):
        self.price_running = False
        self.console.clear()
        self.console.erase()

    def cmd_help(self):
        self.console.add_line('This app allows to read currency data from file and make operations with it', 0)
        self.console.add_line('Commands:', 1)
        self.console.add_line('LIST - shows all ', 1)
        self.console.add_line('PRICE <currency_pair> - shows all ', 1)
        self.console.add_line('BUY <amount> <currency> - shows all ', 1)
        self.console.add_line('SELL <amount> <currency> - shows all ', 1)
        self.console.add_line('QUIT - exit app ', 1)
        # TODO: view mode
        # self.console.add_line('VIEW <mode> - price view mode. "0": new price refresh prev. "1": new lines  ', 1)
        self.console.add_line('---------------------------------------------------------------------------- ', 1)
        self.console.add_line('Timoshchuk Evgeny - timoshchuk.ev@protonmail.com ', 1)

    def cmd_money_operation(self, amount, currency, type):

        if (self.price_running):
            try:
                amount = Decimal(amount)
                if (Decimal(amount) <= 0):
                    self.console.add_line('Error input! Amount must be positive', 2)
                    self.logger.warning('Wrong amount input')
                else:
                    currency_pair = self.currency_pair_queue.get()
                    self.currency_pair_queue.put(currency_pair)
                    if currency == currency_pair[0][0]:
                        counter_amount = Decimal(
                            Decimal(amount) * Decimal(currency_pair[1][1]) / Decimal(currency_pair[0][1]))
                        counter_currency = currency_pair[1][0]
                        output_str = 'USER' + self.operations[type ^ 1] + str(amount) + ' ' + str(currency) + \
                                     self.operations[type] + str(counter_amount) + ' ' + str(counter_currency)
                        self.console.add_line(str(output_str), 1)
                        self.logger.info(str(output_str))
                    elif currency == currency_pair[1][0]:
                        counter_amount = Decimal(
                            Decimal(amount) * Decimal(currency_pair[0][1]) / Decimal(currency_pair[1][1]))
                        counter_currency = currency_pair[0][0]
                        output_str = 'USER' + self.operations[type ^ 1] + str(amount) + ' ' + str(currency) + \
                                     self.operations[type] + str(counter_amount) + ' ' + str(counter_currency)
                        self.console.add_line(output_str, 1)
                        self.logger.info(str(output_str))
                    else:
                        self.logger.warning('Wrong currency input')
                        self.console.add_line('Error. Wrong currency input!', 2)
            # TODO: output with individual precision, connected to input-file currency deviation
            except DecimalException:
                self.console.add_line('Error input! Second argument is not a digit.', 2)
                self.logger.warning('Wrong amount input')
        else:
            self.console.add_line('Choose currency pair by command "PRICE <currency_pair>" first!', 2)

    def cmd_quit(self):
        self.price_running = False
        self.console.add_line('Good bye', 0)
        self.logger.info('App exiting')
        time.sleep(1)
        self.console.exit()
        os._exit(0)

    def logging(self):
        self.logger.setLevel(logging.INFO)
        # create a file handler
        self.logger.handlers = []
        handler = logging.FileHandler('app.log')
        handler.setLevel(logging.INFO)
        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(handler)

    def run(self):

        self.logging()
        self.logger.info('App is started')
        self.console = Console(self.stdscr)
        self.console.add_line('Print csv-file name:', 0)
        while True:
            self.console.read_line()
            self.csv_path = self.console.input_string.strip().lower()
            if self.read_data():
                break

        price_thread = threading.Thread(target=self.price_update)
        price_thread.start()

        try:
            while True:
                self.console.read_line()
                input = self.console.input_string.strip()
                if input.lower() == 'quit':
                    self.logger.info('Command "quit"')
                    self.cmd_quit()
                if input.lower() == 'help':
                    self.logger.info('Command "help"')
                    self.cmd_help()
                if input.split(' ')[0].lower() == 'price':
                    self.logger.info('Command "price"')
                    if len(input.split(' ')) >= 2:
                        self.cmd_price(input.split(' ')[1])
                    else:
                        self.logger.warning('Command "price" without argument')
                        self.console.add_line('Error in command "PRICE". Need second parameter <currency_pair>', 2)
                if input.lower() == 'stop':
                    self.logger.info('Command "stop"')
                    self.price_running = False
                if input.lower() == 'list':
                    self.logger.info('Command "list"')
                    self.cmd_list()
                if input.split(' ')[0].lower() == 'buy':
                    self.logger.info('Command "buy"')
                    if len(input.split(' ')) >= 3:
                        self.cmd_money_operation(input.split(' ')[1], input.split(' ')[2], 1)
                    else:
                        self.logger.warning('Command "buy" without argument')
                        self.console.add_line('Error in command "BUY". Need parameters <amount> and <currency>', 2)
                if input.split(' ')[0].lower() == 'sell':
                    self.logger.info('Command "sell"')
                    if len(input.split(' ')) >= 3:
                        self.cmd_money_operation(input.split(' ')[1], input.split(' ')[2], 0)
                    else:
                        self.logger.warning('Command "sell" without argument')
                        self.console.add_line(
                            'Error in command "SELL". Need required parameters <amount> and <currency>', 2)
        finally:
            self.price_running = False


def main(stdscr):
    app = App(stdscr)
    app.run()


if __name__ == '__main__':
    curses.wrapper(main)
