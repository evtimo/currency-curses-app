# currency-curses-app

Deutsche Bank - test assignment

**Run app:** $ python app.py

**Test scenario:**  
1. Output price and buy 1 amount of currency. Compare in log file cost of this currency on that moment and amount, that user paid. Data must be absolutely equal.  
2. Make refresh interval very small (0.0001s) and wait. Because of negative deviation currency can be negative. This moment must be catched by program exception, cause currency in real life can't be negative.  
3. Test get/put multithreading operations with current currency pair (Readers�writers problem)  
4. Check all money operations for looss of precision  
5. Check all input operations for correct arguments  