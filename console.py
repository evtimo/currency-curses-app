import curses
import os
from curses import textpad


class Console(object):

    def __init__(self, screen=None, prompt_string='>'):
        self.screen = screen
        self.history = []  # TODO: command history
        self.output_window = None
        self.prompt_window = None
        self.prompt_string = prompt_string
        self.initialize()
        self.rebuild_prompt()

    def initialize(self):
        if not self.screen:
            self.screen = curses.initscr()
            curses.noecho()
            curses.cbreak()
        (y, x) = self.screen.getmaxyx()
        self.output_window = self.screen.subwin(y - 2, x, 0, 0)
        self.prompt_window = self.screen.subwin(1, x, y - 2, 0)
        self.edit_filed = textpad.Textbox(self.prompt_window, insert_mode=True)
        self.output_window.scrollok(True)
        self.prompt_window.scrollok(False)

    def rebuild_prompt(self, default_text=None):
        self.prompt_window.clear()
        self.prompt_window.addstr(self.prompt_string)
        if default_text:
            self.prompt_window.addstr(default_text)
        self.prompt_window.refresh()

    def validate_input(self, key):
        if key == curses.KEY_UP:
            pass
            # TODO: handle up and down arrows
        if key == ord('\n'):
            return curses.ascii.BEL
        if key in (curses.ascii.STX, curses.KEY_LEFT, curses.ascii.BS, curses.KEY_BACKSPACE):
            minx = len(self.prompt_string)
            (y, x) = self.prompt_window.getyx()
            if x == minx:
                return None
        return key

    def read_line(self, handle_interrupt=True):
        self.prompt_window.keypad(1)
        try:
            self.input_string = self.edit_filed.edit(self.validate_input)[len(self.prompt_string):]
        except KeyboardInterrupt:
            if handle_interrupt:
                return False
            else:
                raise KeyboardInterrupt
        self.rebuild_prompt()

        return True

    def add_line(self, line, line_break):
        if line_break == 1:
            self.output_window.addstr(line + '\n')
        elif line_break == 2:
            self.output_window.addstr(1, 0, line + '\n')
        else:
            self.output_window.addstr(0, 0, line + '\n')
        self.output_window.refresh()

    def restore_screen(self):
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    def exit(self):
        curses.endwin()
        os._exit(0)


